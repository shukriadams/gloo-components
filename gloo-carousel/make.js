return;
var fs = require('fs'),
    path = require('path');

var css = path.join(__dirname, 'bower_components', 'owl.carousel', 'owl-carousel', 'owl.carousel.css'),
    scss = path.join(__dirname, 'bower_components', 'owl.carousel', 'owl-carousel', 'owl.carousel.scss');

if (!fs.existsSync(css) || fs.existsSync(scss)){
    return;
}

fs.renameSync(css, scss);
