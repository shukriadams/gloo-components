gloo.config({
    paths : {
        'owl.carousel' : 'js/owl.carousel',
        'jquery' : 'bower_components/jquery/jquery'
    },
    shims : {
        'owl.carousel' : { deps : [ 'jquery']}
    }
});

define('gloo-carousel', ['owl.carousel'], function(){

    var Component = function(options){
        $(options.$el).owlCarousel(options);
    };

    return Component;
});